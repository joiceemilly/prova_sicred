package suporte;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Web {
    public static WebDriver createChrome() {
        //Abertura do chrome
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\indra\\drivers\\chromedriver.exe");
        WebDriver navegador = new ChromeDriver();

        // Navegando pela página do projeto
        navegador.get("https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap");

        return navegador;

    }
}
