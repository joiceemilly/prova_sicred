package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class ListarCustomersPage extends BasePage {
    public ListarCustomersPage(WebDriver navegador) {
        super(navegador);
    }
    public AddRecordPage selectUsers(String tipo) {
        WebElement comboSelect = navegador.findElement(By.id("switch-version-select"));
        Select combobox = new Select((comboSelect));
        combobox.selectByVisibleText("Bootstrap V4 Theme");

        navegador.findElement(By.linkText("Add Record")).click();

        return new AddRecordPage(navegador);
    }
}
