package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

    public class AddRecordPage extends BasePage {
        public AddRecordPage(WebDriver navegador) {
            super(navegador);
    }
    public AddRecordPage CustomerName() {
        WebElement inputCustomerName = navegador.findElement(By.id("field-customerName"));
        inputCustomerName.sendKeys("Teste Sicredi");

        return this;
        }
    public AddRecordPage CustomerLastName(){
        WebElement inputContactLastName = navegador.findElement(By.id("field-contactLastName"));
        inputContactLastName.sendKeys("Teste");

        return this;
    }
    public AddRecordPage CustomerFirstName(){
        WebElement inputContactFirstName = navegador.findElement(By.id("field-contactFirstName"));
        inputContactFirstName.sendKeys("Joice");

        return this;
    }
    public AddRecordPage CustomerPhone (){
        WebElement inputPhone = navegador.findElement(By.id("field-phone"));
        inputPhone.sendKeys("51 9999-9999");

        return this;
    }
    public AddRecordPage CustomerAddressOne(){
        WebElement inputAddressLineOne = navegador.findElement(By.id("field-addressLine1"));
        inputAddressLineOne.sendKeys("Av Assis Brasil, 3970");

        return this;
    }
    public AddRecordPage CustomerAddressTwo(){
        WebElement inputAddressLineTwo = navegador.findElement(By.id("field-addressLine2"));
        inputAddressLineTwo.sendKeys("Torre D");

        return this;
    }
    public AddRecordPage CustomerCity(){
        WebElement inputCity = navegador.findElement(By.id("field-city"));
        inputCity.sendKeys("Porto Alegre");

        return this;
    }
    public AddRecordPage CustomerState(){
        WebElement inputState = navegador.findElement(By.id("field-state"));
        inputState.sendKeys("RS");

        return this;
    }
    public AddRecordPage CustomerPostalCode(){
        WebElement inputPostalCode = navegador.findElement(By.id("field-postalCode"));
        inputPostalCode.sendKeys("91000-000");

        return this;
    }
    public AddRecordPage CustomerCountry(){
        WebElement inputCountry = navegador.findElement(By.id("field-country"));
        inputCountry.sendKeys("Brasil");

        return this;
    }
    public AddRecordPage SalesRepEmployeeNumber(){
        WebElement inputSalesRep = navegador.findElement(By.id("field-salesRepEmployeeNumber"));
        inputSalesRep.sendKeys("Fixter");

        return this;
    }
    public AddRecordPage CustomerCreditLimit(){
            WebElement inputCreditLimit = navegador.findElement(By.id("field-creditLimit"));
            inputCreditLimit.sendKeys("200");
        return this;
    }
    public AddRecordPage CustomerSave(){
        navegador.findElement(By.id("form-button-save")).click();

        return this;
        }
    }