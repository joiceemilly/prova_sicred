package tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.ListarCustomersPage;
import suporte.Generator;
import suporte.Screenshot;
import suporte.Web;


public class AddCustomerTest {
    private WebDriver navegador;

    @Rule
    public TestName dbcTest = new TestName();

    @Before
    public void setUp(){
        navegador = Web.createChrome();
    }
    @Test
    public void testAdClient() {
         new ListarCustomersPage(navegador)
                .selectUsers("Bootstrap V4 Theme")
                .CustomerName()
                .CustomerLastName()
                .CustomerFirstName()
                .CustomerPhone()
                .CustomerAddressOne()
                .CustomerAddressTwo()
                .CustomerCity()
                .CustomerState()
                .CustomerPostalCode()
                .CustomerCountry()
                .SalesRepEmployeeNumber()
                .CustomerCreditLimit()
                .CustomerSave()
                .capturarTextoCustomerSalvo();

        //Implementação de tempo de espera para a validação da mensagem de cadastro
        WebDriverWait wait = new WebDriverWait(navegador, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("report-success")));
        assertEquals("Your data has been successfully stored into the database. Edit Record or Go back to list", navegador.findElement(By.id("report-success")).getText());

       //Screenshots para evidências dos resultados dos testes
        String screenshotArquivo = "C:\\Users\\indra\\testsreport\\AddCustomer\\" + Generator.datahoraParaArquivo() + dbcTest.getMethodName() + ".png";
        Screenshot.print(navegador, screenshotArquivo);

    }

    @After
    public void tearDown(){
        //Fechar o navegador
        navegador.close();
    }
}