## Teste_SICRED
 
... Esse readme explica como funciona a automação para a execução dos testes para a DBC company.
 
 
## Tecnologias
 
Tecnologias utilizadas para o desenvolvimento da automação
 
* Java SE 1.8.0
* IntelliJ Idea Comunnity Edition 2021.2.2
* JUnit versão 4.12 - suas dependências constam no arquivo pom.xml
* Selenium Versão 3.6.0 - suas dependências constam no arquivo pom.xml
- 
* Commons-io Versão 2.6 - suas dependências constam no arquivo pom.xml
 


 
## Como executar os testes
Ao realizar a abertura do projeto na IDE, no arquivo **"Tests"** buscar pela Java class "**AddCustomerTest**".

A execução estará logo na primeira public class "**AddCustomerTests**".

- **Na automação está implentado também Screenshots para evidênciação das validações no cadastro do customer. Para uma execução correta com a evidênciação das validações, recomenda-se a adaptação do path dos arquivos de acordo com a explicação abaixo:**

*Dentro da sua pasta do usuário no sistema operacional, adicione uma pasta de nome: "**testsreport**";

*Dentro da pasta **"testsreport"**, adicione uma pasta "**AddCustomer**" para a evidênciação única e exclusica da funcionalidade de cadastro de novos customers.

*Entre na pasta "**AddCustomers**" e copie o caminho da pasta.

*Na IDE utilizada para a automação, IntelliJ Idea, dirija-se ao arquivo "**AddCustomerTest**" e na linha 55 adicione o caminho da sua pasta "**AddCustomer**". 

Exemplo: String screenshotArquivo = "C:\\Users\\DBC\\testsreport\\AddCustomer\\" + Generator.datahoraParaArquivo() + dbcTest.getMethodName() + ".png";

Lembrar de sempre colocar contra-barra dupla, caso o sistema operacional seja windows.

      

 
 
## Authors
Joice Emilly Nascimento da Silva.
